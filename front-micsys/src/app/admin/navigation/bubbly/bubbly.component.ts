import { SharedService } from './../shared.service';
import { Component, OnInit } from '@angular/core';
import { Menuitem } from './MenuItems';

@Component({
  selector: 'app-bubbly',
  templateUrl: './bubbly.component.html',
  styleUrls: ['./bubbly.component.red.scss']
})
export class BubblyComponent implements OnInit {
  toggleActivator = false;
  menuItems = Menuitem.menuItems;
  constructor(private shared: SharedService) { }

  ngOnInit() {
    this.shared.getToggle$().subscribe(
      res => {
        this.toggleActivator = res;
      }
    );
  }
}
