export class Menuitem {
  public static iconsPlatforms = {
    md: 'ion-md-',
    ios: 'ion-ios-',
    logo: 'ion-logo-'
  };
  public static menuItems = [
    {
      route: 'clients',
      ionicon:
        {
          icon: 'home',
          platform: Menuitem.iconsPlatforms.md
        }
      ,
      faicon:
        {
          name: 'address-book',
          animate: '',
          size: '2x'
        }
      ,
      title: 'Clients',
      showIon: false
    },
    {
      route: 'projects',
      ionicon:
        {
          icon: 'clipboard',
          platform: Menuitem.iconsPlatforms.md
        }
      ,
      faicon:
        {
          name: 'archive',
          animate: '',
          size: '2x'
        }
      ,
      title: 'Projects',
      showIon: false
    },
    {
      route: 'board',
      ionicon:
        {
          icon: 'pricetags',
          platform: Menuitem.iconsPlatforms.ios
        }
      ,
      faicon:
        {
          name: 'ticket',
          animate: '',
          size: '2x'
        }
      ,
      title: 'Board',
      showIon: false
    },
    {
      route: 'resources',
      ionicon:
        {
          icon: 'leaf',
          platform: Menuitem.iconsPlatforms.ios
        }
      ,
      faicon:
        {
          name: 'leaf',
          animate: '',
          size: '2x'
        }
      ,
      title: 'Resources',
      showIon: false
    },
    {
      route: 'charts',
      ionicon:
        {
          icon: 'trending-up',
          platform: Menuitem.iconsPlatforms.ios
        }
      ,
      faicon:
        {
          name: 'line-chart',
          animate: '',
          size: '2x'
        }
      ,
      title: 'Charts',
      showIon: false
    },
    {
      route: 'settings',
      ionicon:
        {
          icon: 'settings',
          platform: Menuitem.iconsPlatforms.ios
        }
      ,
      faicon:
        {
          name: 'cog',
          animate: '',
          size: '2x'
        }
      ,
      title: 'Settings',
      showIon: false
    }
  ];
}
