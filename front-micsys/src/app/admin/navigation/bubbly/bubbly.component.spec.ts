import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BubblyComponent } from './bubbly.component';

describe('BubblyComponent', () => {
  let component: BubblyComponent;
  let fixture: ComponentFixture<BubblyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BubblyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BubblyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
