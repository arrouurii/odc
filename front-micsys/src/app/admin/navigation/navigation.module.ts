import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavigationRoutingModule } from './navigation-routing.module';
import { BubblyComponent } from './bubbly/bubbly.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NavigationComponent } from './navigation.component';

@NgModule({
  declarations: [BubblyComponent, NavbarComponent, NavigationComponent],
  imports: [
    CommonModule,
    NavigationRoutingModule,
    AngularFontAwesomeModule
  ]
})
export class NavigationModule { }
