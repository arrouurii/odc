import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private toggleActivator = new BehaviorSubject<boolean>(false);
  constructor() { }
  toggle(value: boolean) {
    this.toggleActivator.next(value);
  }

  getToggle$() {
    return this.toggleActivator.asObservable();
  }
}
