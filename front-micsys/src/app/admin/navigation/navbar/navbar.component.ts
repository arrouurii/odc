import { SharedService } from './../shared.service';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import {AuthenticationService} from '../../authentication/services/authentication.service';
import {Router} from '@angular/router';
import { AppConstants } from 'src/app/AppConstants';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  username: string;
  constructor(private shared: SharedService, private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.username = this.auth.currentUserValue.username;
    // console.log(this.auth.getUsername());
  }
  toggleClass(event: Event) {
    this.shared.getToggle$().pipe(first()).subscribe(
      res => {
        this.shared.toggle(!res);
      }
    );
  }
  logout() {
    this.auth.logout();
    this.router.navigateByUrl(AppConstants.ADMIN_LOGIN_URL);
  }
}
