import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientManagerRoutingModule } from './client-manager-routing.module';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientElementComponent } from './client-element/client-element.component';
import { ClientProjectsComponent } from './client-element/client-projects/client-projects.component';
import { ClientReclamationsComponent } from './client-element/client-reclamations/client-reclamations.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ClientAddNewComponent } from './client-element/client-add-new/client-add-new.component';
import { ClientEditComponent } from './client-element/client-edit/client-edit.component';

@NgModule({
  declarations: [
    ClientsListComponent,
    ClientElementComponent,
    ClientProjectsComponent,
    ClientReclamationsComponent,
    ClientAddNewComponent,
    ClientEditComponent
  ],
  imports: [
    CommonModule,
    ClientManagerRoutingModule,
    AngularFontAwesomeModule,
    SharedModule
  ]
})
export class ClientManagerModule { }
