import { Observable } from 'rxjs';
import { AppConstants } from 'src/app/AppConstants';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../../authentication/services/authentication.service';

export interface SortObject {
  sortBy: string;
  sortOrder: string;
}

export interface PageObject {
  page: number;
  size: number;
}
@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private http: HttpClient, private auth: AuthenticationService) { }

  getClients(sort?: SortObject, pagination?: any): Observable<any> {
    if (sort && !pagination) {
      return this.http.get(AppConstants.API_URL + '/clients?sort=' + sort.sortBy + ',' + sort.sortOrder);
    } else if (!sort && pagination) {
      return this.http.get(AppConstants.API_URL + '/clients?page=' + pagination.page + '&size=' + pagination.size);
    } else if (sort && pagination) {
      return this.http.get(
        AppConstants.API_URL + '/clients?page=' + pagination.page + '&size='
        + pagination.size + '&sort=' + sort.sortBy + ',' + sort.sortOrder
      );
    }
    return this.http.get(AppConstants.API_URL + '/clients');
  }
  // -------------------------------------------------------
  getClient(ressourceUrl: string): Observable<any> {
    return this.getResource(ressourceUrl);
  }
  // -------------------------------------------------------
  addClient(body: any): Observable<any> {
    return this.postResource(AppConstants.API_URL + '/clients/signup', body);
  }
  // -------------------------------------------------------
  getResource(ressourceUrl: string): Observable<any> {
    return this.http.get(ressourceUrl);
  }
  // -------------------------------------------------------
  postResource(ressourceUrl: string, body: any): Observable<any> {
    return this.http.post(ressourceUrl, body, {observe: 'response'});
  }
  // -------------------------------------------------------
  putResource(ressourceUrl: string, body: any): Observable<any> {
    return this.http.put(ressourceUrl, body, {observe: 'response'});
  }
  // -------------------------------------------------------
  patchResource(ressourceUrl: string, body: any): Observable<any> {
    return this.http.patch(ressourceUrl, body, {observe: 'response'});
  }
  // -------------------------------------------------------
  deleteResource(ressourceUrl: string): Observable<any> {
    return this.http.delete(ressourceUrl, {observe: 'response'});
  }
}
