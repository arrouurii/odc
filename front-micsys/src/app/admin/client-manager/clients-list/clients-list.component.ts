import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../services/clients.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {
  page: any;
  currentPage = 0;
  size = 5;
  clients = [];

  loading = false;
  errorOccured = false;
  errorMessage = '';

  form = new FormGroup({});
  // -----------------------------------
  constructor(
    private clientService: ClientsService,
    private router: Router,
    private notifier: NotifierService
  ) { }
  // ------------------------
  ngOnInit() {
    this.form = new FormGroup({
      sortBy: new FormControl('sort by'),
      sortOrder: new FormControl('asc'),
    });
    this.getClients(this.form.value, {page: this.currentPage, size: this.size});
  }
  // -------------------------
  showElement(client: any) {
    const url = btoa(client._links.self.href);
    this.router.navigateByUrl('/admin/msdesk/clients/list/' + url)
    .then(
      () => {
        console.log('done');
      }
    ).catch(
      e => console.log(e + 'ERROR')
    );
  }
  // --------------------------
  getClients(sortBy?: any, pagination?: any) {
    this.loading = true;
    this.clientService.getClients(sortBy, pagination)
      .subscribe(
        data => {
          this.errorOccured = false;
          this.errorMessage = '';
          // -------------------
          this.page = data.page;
          this.clients = data._embedded.clients;
          // -------------------
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.errorMessage = 'Error while importing data, verify your connexion please';
        }
      );
  }
  // --------------------------
  sortBy() {
    this.getClients(this.form.value, {page: this.currentPage, size: this.size});
  }
  // --------------------------
  addNewClient() {
    this.router.navigateByUrl('/admin/msdesk/clients/add');
  }
  // --------------------------
  editClient(client: any) {
    const url = btoa(client._links.self.href);
    this.router.navigateByUrl('/admin/msdesk/clients/edit/' + url)
    .then(
      () => {
        console.log('done');
      }
    ).catch(
      e => console.log(e + 'ERROR')
    );
  }
  // --------------------------
  deleteClient(client: any) {
    this.clientService.deleteResource(client._links.self.href)
      .subscribe(
        response => {
          this.getClients();
          this.notifier.notify('success', client.username + ' has been deleted successfully !');
        },
        error => {
          if (error.status === 409) {
            this.notifier.notify('error', 'an error has occured while deleting' + client.username + ' ,' + error.erroe.cause.cause.message);
          }
          this.notifier.notify('error', 'an error has occured while deleting' + client.username);
        }
      );
  }
  // --------------------------
  nextPage() {
    if (this.currentPage < (this.page.totalPages - 1)) {
      this.currentPage++;
      console.log('next', this.form.value);
      this.getClients(this.form.value, {page: this.currentPage, size: this.size});
    }
  }
  // --------------------------
  previousPage() {
    if (this.currentPage > 0) {
      this.currentPage--;
      console.log('prev', this.form.value);
      this.getClients(this.form.value, {page: this.currentPage, size: this.size});
    }
  }
  // --------------------------
  orderToggle() {
    const value = this.form.get(['sortOrder']).value;
    if (value === 'asc') {
      this.form.get(['sortOrder']).setValue('desc');
    } else if (value === 'desc') {
      this.form.get(['sortOrder']).setValue('asc');
    }
    this.sortBy();
  }

}
