import { ClientEditComponent } from './client-element/client-edit/client-edit.component';
import { ClientAddNewComponent } from './client-element/client-add-new/client-add-new.component';
import { ClientElementComponent } from './client-element/client-element.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '', component: ClientsListComponent},
  {path: 'list/:url', component: ClientElementComponent},
  {path: 'add', component: ClientAddNewComponent},
  {path: 'edit/:url', component: ClientEditComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientManagerRoutingModule { }
