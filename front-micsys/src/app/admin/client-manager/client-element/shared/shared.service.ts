import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  private clientSubject = new BehaviorSubject<any>(null);
  private clientProjects = [];
  private clientReclamations = [];
  constructor() { }

  getClientProjects() {
    return this.clientProjects;
  }

  getclientReclamations() {
    return this.clientReclamations;
  }

  getClient() {
    return this.clientSubject.value;
  }

  setClient(client) {
    this.clientSubject.next(client);
  }

  getClientSubject() {
    return this.clientSubject;
  }
}
