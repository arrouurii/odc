import { Client } from './../../../models/Client';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SharedService } from '../shared/shared.service';
import { ActivatedRoute } from '@angular/router';
import { ClientsService } from '../../services/clients.service';
import { TouchSequence } from 'selenium-webdriver';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.scss']
})
export class ClientEditComponent implements OnInit {
  form = new FormGroup({});
  loading = false;
  errorOccured = false;
  message = '';
  client: any;
  // -------------------------------
  constructor(
    private clientService: ClientsService,
    private activeRoute: ActivatedRoute,
    private shared: SharedService,
    private notifier: NotifierService
  ) { }

  ngOnInit() {
    this.form = new FormGroup(
      {
        firstName: new FormControl(null, [Validators.required, Validators.minLength(2)]),
        lastName: new FormControl(null, [Validators.required, Validators.minLength(2)]),
        username: new FormControl(null, [Validators.required, Validators.minLength(3)]),
        email: new FormControl(null, [
          Validators.required,
          Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')
        ]),
        gender: new FormControl(null, [Validators.required]),
        position: new FormControl('Individual', [Validators.required, Validators.pattern('^(Individual|Enterprise)$')]),
        type: new FormControl('Owner', [Validators.required, Validators.pattern('^(Representative|Owner)$')]),
        enterpriseName: new FormControl(null),
        activate: new FormControl(null)
      }
    );

    this.getElement();
  }
  // -----------------------------
  getElement() {
    this.loading = true;
    this.errorOccured = false;
    this.message = '';
    // *******************
    const url: string = atob(this.activeRoute.snapshot.params.url);
    this.clientService.getClient(url)
      .subscribe(
        data => {
          this.client = data;
          console.log('DATA :: ', data);
          const value = {
            email: data.email,
            firstName: data.firstName,
            lastName: data.lastName,
            username: data.username,
            gender: data.gender,
            position: data.position,
            type: data.type,
            enterpriseName: data.enterpriseName,
            activate: data.activate
          };
          this.form.setValue(value);
          console.warn('form value', this.form.value);
          this.shared.setClient(this.client);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.errorOccured = true;
          this.message = error;
          console.log(error);
        }
      );
  }
  // ----------------------------------------------
  editClient() {
    this.loading = true;
    this.clientService.patchResource(this.client._links.self.href, this.form.value)
      .subscribe(
        response => {
          this.notifier.notify('success', 'Edited Successfully');
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.notifier.notify('error', error.error.cause.cause.message);
        }
      );
  }
}
