import { SharedService } from './shared/shared.service';
import { ClientsService } from './../services/clients.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-client-element',
  templateUrl: './client-element.component.html',
  styleUrls: ['./client-element.component.scss']
})
export class ClientElementComponent implements OnInit {
  loading = false;
  errorOccured = false;
  errorMessage = '';
  client: any;
  constructor(
    private clientService: ClientsService,
    private activeRoute: ActivatedRoute,
    private shared: SharedService
  ) { }

  ngOnInit() {
    this.getElement();
  }

  getElement() {
    this.loading = true;
    this.errorOccured = false;
    this.errorMessage = '';
    // *******************
    const url: string = atob(this.activeRoute.snapshot.params.url);
    this.clientService.getClient(url)
      .subscribe(
        data => {
          this.client = data;
          this.shared.setClient(this.client);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.errorOccured = true;
          this.errorMessage = error;
          console.log(error);
        }
      );
  }
}
