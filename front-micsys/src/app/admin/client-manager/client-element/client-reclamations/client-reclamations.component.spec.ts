import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientReclamationsComponent } from './client-reclamations.component';

describe('ClientReclamationsComponent', () => {
  let component: ClientReclamationsComponent;
  let fixture: ComponentFixture<ClientReclamationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientReclamationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientReclamationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
