import { ClientsService } from './../../services/clients.service';
import { SharedService } from './../shared/shared.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-client-reclamations',
  templateUrl: './client-reclamations.component.html',
  styleUrls: ['./client-reclamations.component.scss']
})
export class ClientReclamationsComponent implements OnInit {
  reclamations = [];
  constructor(public shared: SharedService, private clientService: ClientsService) { }

  ngOnInit() {
    this.shared.getClientSubject()
      .subscribe(
        client => {
          if (client) {
            const url = client._links.reclamations.href;
            this.getClientProjects(url);
          }
        }
      );
  }
  // -------------------------------------------
  // --------------------------------------------------------------
  getClientProjects(url: string) {
    return this.clientService.getResource(url)
      .subscribe(
        reclamations => {
          this.reclamations = reclamations._embedded.reclamations;
          console.log('projects', reclamations);
        }
      );
  }
}
