import { ClientsService } from './../../services/clients.service';
import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared/shared.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-client-projects',
  templateUrl: './client-projects.component.html',
  styleUrls: ['./client-projects.component.scss']
})
export class ClientProjectsComponent implements OnInit {
  public projects = [];
  constructor(public shared: SharedService, private clientService: ClientsService) { }

  ngOnInit() {
    this.shared.getClientSubject()
      .subscribe(
        client => {
          if (client) {
            const url = client._links.projects.href;
            this.getClientProjects(url);
          }
        }
      );
  }
  // --------------------------------------------------------------
  getClientProjects(url: string) {
    return this.clientService.getResource(url)
      .subscribe(
        projects => {
          this.projects = projects._embedded.projects;
          console.log('projects', projects);
        }
      );
  }
}
