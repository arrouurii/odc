import { NotifierService } from 'angular-notifier';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClientsService } from '../../services/clients.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-add-new',
  templateUrl: './client-add-new.component.html',
  styleUrls: ['./client-add-new.component.scss']
})
export class ClientAddNewComponent implements OnInit {
  form = new FormGroup({});
  loading = false;
  errorOccured = false;
  message = '';
  constructor(
    private clientService: ClientsService,
    private router: Router,
    private notifier: NotifierService
  ) { }

  ngOnInit() {
    this.form = new FormGroup(
      {
        firstName: new FormControl(null, [Validators.required, Validators.minLength(2)]),
        lastName: new FormControl(null, [Validators.required, Validators.minLength(2)]),
        username: new FormControl(null, [Validators.required, Validators.minLength(3)]),
        email: new FormControl(null, [
          Validators.required,
          Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')
        ]),
        password: new FormControl(null, [
          Validators.required,
          Validators.pattern('(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$')
        ]),
        confirmPassword: new FormControl(null, [
          Validators.required,
          Validators.pattern('(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$')
        ]),
        gender: new FormControl('0', [Validators.required]),
        position: new FormControl('Individual', [Validators.required, Validators.pattern('^(Individual|Enterprise)$')]),
        type: new FormControl('Owner', [Validators.required, Validators.pattern('^(Representative|Owner)$')]),
        enterpriseName: new FormControl(null)
      }
    );
  }
  // ----------------------------------------------------------
  addUser() {
    this.loading = true;
    this.clientService.addClient(this.form.value)
      .subscribe(
        response => {
          this.message = 'User scuccessfuly added';
          this.errorOccured = false;
          this.loading = false;
          this.notifier.notify('success', this.message);
          this.router.navigateByUrl('/admin/msdesk/clients');
        },
        error => {
          console.log(error);
          this.loading = false;
          this.errorOccured = true;
          this.message = 'Sorry, an error has occured. Try again';
          this.notifier.notify('error', this.message);
        }
      );
  }
  // ---------------------------------------------------------------
  cancel() {
    this.router.navigateByUrl('/admin/msdesk/clients');
  }
}
