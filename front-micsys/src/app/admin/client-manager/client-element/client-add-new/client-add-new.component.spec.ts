import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAddNewComponent } from './client-add-new.component';

describe('ClientAddNewComponent', () => {
  let component: ClientAddNewComponent;
  let fixture: ComponentFixture<ClientAddNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientAddNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAddNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
