import { SharedService } from './../services/shared.service';
import { AccessService } from './../services/access.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-username-challenge',
  templateUrl: './username-challenge.component.html',
  styleUrls: ['./username-challenge.component.scss']
})
export class UsernameChallengeComponent implements OnInit {
  form: FormGroup = new FormGroup({
    username: new FormControl(null, Validators.required)
  });
  isAdmin = false;
  loading = false;
  errorOccured = false;
  errorMessage = '';
  constructor(private access: AccessService, public shared: SharedService) { }

  ngOnInit() {
  }

  verifyUsername(): void {
    this.loading = true;
    this.access.verifyUsername(this.form.value.username)
    .subscribe(
      data => {
        this.shared.setIsAdmin(data.body);
        this.isAdmin = this.shared.getIsAdmin();
        this.loading = false;
        this.errorOccured = false;
        this.errorMessage = '';
      },
      error => {
        this.errorMessage = 'Verify your username, please';
        this.errorOccured = true;
        this.shared.setIsAdmin(false);
        this.isAdmin = false;
        this.loading = false;
      }
    );
  }

}
