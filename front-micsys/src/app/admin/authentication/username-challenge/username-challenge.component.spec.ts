import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsernameChallengeComponent } from './username-challenge.component';

describe('UsernameChallengeComponent', () => {
  let component: UsernameChallengeComponent;
  let fixture: ComponentFixture<UsernameChallengeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsernameChallengeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsernameChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
