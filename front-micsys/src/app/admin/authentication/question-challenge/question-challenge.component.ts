import { SharedService } from './../services/shared.service';
import { AccessService } from './../services/access.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-question-challenge',
  templateUrl: './question-challenge.component.html',
  styleUrls: ['./question-challenge.component.scss']
})
export class QuestionChallengeComponent implements OnInit {
  form: FormGroup = new FormGroup({
    answer: new FormControl(null, Validators.required)
  });
  loading = false;
  errorOccured = false;
  errorMessage = '';
  isAdmin = false;
  challengeIsValid = false;
  constructor(private access: AccessService, public shared: SharedService) { }

  ngOnInit() {
    this.isAdmin = this.shared.getIsAdmin();
  }

  verifyAnswer(): void {
    const answer = +this.form.value.answer;
    this.isAdmin = this.shared.getIsAdmin();
    this.loading = true;
    this.errorMessage = '';
    this.errorOccured = false;
    this.access.verifyAnswer(answer)
    .subscribe(
      data => {
        if (! this.isAdmin) { throw new Error('Error :: wrong flow'); }
        this.shared.setChallengeIsValid(true);
        this.challengeIsValid = this.shared.getChallengeIsValid();
        this.loading = false;
      },
      error => {
        this.loading = false;
        this.errorOccured = true;
        this.errorMessage = 'Are you sure ?';
      }
    );
  }
}
