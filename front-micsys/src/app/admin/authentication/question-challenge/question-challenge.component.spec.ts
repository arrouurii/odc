import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionChallengeComponent } from './question-challenge.component';

describe('QuestionChallengeComponent', () => {
  let component: QuestionChallengeComponent;
  let fixture: ComponentFixture<QuestionChallengeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionChallengeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
