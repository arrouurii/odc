import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { LoginComponent } from './login/login.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { UsernameChallengeComponent } from './username-challenge/username-challenge.component';
import { QuestionChallengeComponent } from './question-challenge/question-challenge.component';
import { ContainerComponent } from './container/container.component';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [LoginComponent, UsernameChallengeComponent, QuestionChallengeComponent, ContainerComponent],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class AuthenticationModule { }
