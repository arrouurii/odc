import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConstants } from 'src/app/AppConstants';

@Injectable({
  providedIn: 'root'
})
export class AccessService {

  constructor(private http: HttpClient) { }

  verifyUsername(username: string): Observable<HttpResponse<boolean>> {
    return this.http.post<boolean>(AppConstants.API_URL + '/admin/ami', username, {observe: 'response'});
  }

  verifyAnswer(answer: number): Observable<HttpResponse<boolean>> {
    return this.http.post<boolean>(AppConstants.API_URL + '/admin/challenge', answer, {observe: 'response'});
  }
}
