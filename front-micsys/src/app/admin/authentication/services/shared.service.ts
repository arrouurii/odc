import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  private isAdmin = false;
  private challengeIsValid = false;
  constructor() { }

  getIsAdmin(): boolean {return this.isAdmin; }
  setIsAdmin(isAdmin: boolean): void { this.isAdmin = isAdmin; }

  getChallengeIsValid(): boolean {return this.challengeIsValid; }
  setChallengeIsValid(challengeIsValid: boolean): void {this.challengeIsValid = challengeIsValid; }
}
