import { AppConstants } from 'src/app/AppConstants';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../models/User';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  private token: string;
  private username: string;
  private roles: Array<string>;
  private expiration: number;
  private issuedAt: number;
  // =====================================
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  decodeJWT(token: string) {
    this.token = token;
    // this.saveToken(token);
    const jwtHelper = new JwtHelperService();
    const objJWT = jwtHelper.decodeToken(this.token);
    // ---------------------------
    this.username = objJWT.sub;
    this.roles = objJWT.roles;
    this.expiration = objJWT.exp * 1000;
    this.issuedAt = objJWT.iat * 1000;

  }

  login(username: string, password: string) {
    return this.http.post<any>(AppConstants.API_URL + '/login', {'username': username, 'password': password}, {observe: 'response'})
    .pipe(
      map(data => {
        console.log(data);
        this.token = data.headers.get('Authorization').split(' ')[1];
        this.decodeJWT(this.token);
        const verify = this.token !== null
          && this.username !== null
          && this.isAdmin()
          && this.expiration > this.issuedAt
          && this.expiration > (new Date()).getTime();
        const user: User = new User();
        console.log('verify', verify);
        if (verify) {
          user.token = this.token;
          user.username = this.username;
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }
        return user;
      })
    );
  }
  // =====================================
  logout(): void {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
  // =====================================
  isAdmin() {
    return this.roles.indexOf('ADMIN') >= 0;
  }
  // =====================================
  isUser() {
    return this.roles.indexOf('USER') >= 0;
  }
}
