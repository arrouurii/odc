import { AccessService } from './../services/access.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {
  public isAdmin = false;
  public challengeResolved = false;
  constructor() { }

  ngOnInit() {
  }

}
