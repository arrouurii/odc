import { AppConstants } from 'src/app/AppConstants';
import { AuthenticationService } from './../services/authentication.service';
import { SharedService } from './../services/shared.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  loading = false;
  errorOccured = false;
  errorMesage = '';
  constructor(public shared: SharedService, private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.form = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
    if (this.auth.currentUserValue) {
      this.router.navigateByUrl(AppConstants.ADMIN_HOME_URL);
    } else { this.auth.logout(); }
  }
  login() {
    this.loading = true;
    this.auth.login(this.form.value.username, this.form.value.password)
      .pipe(first())
      .subscribe(
        data => {
          this.errorOccured = false;
          this.errorMesage = '';
          this.loading = false;
          this.router.navigateByUrl(AppConstants.ADMIN_HOME_URL);
        },
        error => {
          this.loading = false;
          this.errorOccured = true;
          this.errorMesage = 'Bad credentials';
          console.error('ERROR :: ', error);
        }
      );
  }
}
