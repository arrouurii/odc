export class Client {
  email: string;
  firstName: string;
  lastName: string;
  usename: string;
  gender: string;
  position: string;
  type: string;
  enterpriseName: string;
  activate: string;
}
