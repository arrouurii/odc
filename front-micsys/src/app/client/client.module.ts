import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import { MainContainerComponent } from './main-container/main-container.component';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ReclamationsListComponent } from './reclamations-list/reclamations-list.component';

@NgModule({
  declarations: [MainContainerComponent, ProjectsListComponent, ReclamationsListComponent],
  imports: [
    CommonModule,
    ClientRoutingModule,
    AngularFontAwesomeModule
  ]
})
export class ClientModule { }
