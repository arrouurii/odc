export abstract class AppConstants {
  public static readonly API_URL = 'http://localhost:8080';
  public static readonly ADMIN_LOGIN_URL = '/admin/msdesk/auth/login';
  public static readonly ADMIN_HOME_URL = '/admin/msdesk';
}
