import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from '../admin/authentication/loading/loading.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotifierModule } from 'angular-notifier';
import { PaginationComponent } from './pagination/pagination.component';

@NgModule({
  declarations: [
    LoadingComponent,
    PaginationComponent
  ],
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NotifierModule
  ],
  exports: [
    LoadingComponent,
    ReactiveFormsModule,
    FormsModule,
    AngularFontAwesomeModule,
    NotifierModule
  ]
})
export class SharedModule { }
