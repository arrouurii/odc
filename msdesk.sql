-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2019 at 11:03 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `msdesk`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_role`
--

CREATE TABLE `app_role` (
  `id` bigint(20) NOT NULL,
  `role_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_role`
--

INSERT INTO `app_role` (`id`, `role_name`) VALUES
(2, 'ADMIN'),
(1, 'USER');

-- --------------------------------------------------------

--
-- Table structure for table `app_user`
--

CREATE TABLE `app_user` (
  `id` bigint(20) NOT NULL,
  `activate` bit(1) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `gender` int(11) NOT NULL,
  `inscription_date` date NOT NULL,
  `last_connexion` date NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_user`
--

INSERT INTO `app_user` (`id`, `activate`, `email`, `first_name`, `gender`, `inscription_date`, `last_connexion`, `last_name`, `password`, `username`) VALUES
(1, b'1', NULL, 'Admin', 0, '2019-05-07', '2019-05-07', 'Admin', '$2a$10$lA.SZE7dpPwHrPuRxHPR.uyjgQ41bLGkd2YK7wvX.cOOXO.Ng1ccq', 'admin'),
(2, b'1', NULL, 'Client', 0, '2019-05-07', '2019-05-07', 'ali', '$2a$10$N8Q5gBml4Pq4VrHxRDnJM.cl3Pc1ASJyu1nS1SaeDQl3sUepUB7Oe', 'wa7de5er'),
(3, b'1', NULL, 'Client', 0, '2019-05-07', '2019-05-07', 'Nidhal', '$2a$10$/dGCRFhxdsu37u7Zrp961.AL1H2Zut3qGCYhJw7zbzQiBWh3LAMBq', 'nidhal'),
(4, b'1', NULL, 'Client', 0, '2019-05-07', '2019-05-07', 'Aymen', '$2a$10$Z3GFIXtqN4YvdreBWQbgruAVOp2Bpq3TEZ3qpQ694diJIIVLu8Gdu', 'aymen');

-- --------------------------------------------------------

--
-- Table structure for table `app_user_roles`
--

CREATE TABLE `app_user_roles` (
  `app_user_id` bigint(20) NOT NULL,
  `roles_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_user_roles`
--

INSERT INTO `app_user_roles` (`app_user_id`, `roles_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `position` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  `enterprise_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`position`, `type`, `id`, `enterprise_name`) VALUES
(NULL, NULL, 2, NULL),
(NULL, NULL, 3, NULL),
(NULL, NULL, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_projects`
--

CREATE TABLE `client_projects` (
  `client_id` bigint(20) NOT NULL,
  `projects_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_projects`
--

INSERT INTO `client_projects` (`client_id`, `projects_id`) VALUES
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `client_reclamations`
--

CREATE TABLE `client_reclamations` (
  `client_id` bigint(20) NOT NULL,
  `reclamations_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_reclamations`
--

INSERT INTO `client_reclamations` (`client_id`, `reclamations_id`) VALUES
(3, 7),
(3, 8),
(3, 9),
(3, 11),
(3, 12);

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequences`
--

CREATE TABLE `hibernate_sequences` (
  `sequence_name` varchar(255) NOT NULL,
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hibernate_sequences`
--

INSERT INTO `hibernate_sequences` (`sequence_name`, `next_val`) VALUES
('default', 2);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` bigint(20) NOT NULL,
  `guarantee_duration_in_months` int(11) DEFAULT NULL,
  `last_update` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `guarantee_duration_in_months`, `last_update`, `name`, `release_date`, `version`, `client_id`) VALUES
(1, 48, '2019-05-05', 'MSDESK', '2019-05-05', '0.0.1-SNAPSHOT', 3),
(3, 48, '2019-05-05', 'MSDESK', '2019-05-05', '0.0.1-SNAPSHOT', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reclamation`
--

CREATE TABLE `reclamation` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `reclamation_date` date DEFAULT NULL,
  `resolution_date` date DEFAULT NULL,
  `severity` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `reclaimer_id` bigint(20) DEFAULT NULL,
  `reclamations_id` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reclamation`
--

INSERT INTO `reclamation` (`id`, `name`, `reclamation_date`, `resolution_date`, `severity`, `type`, `project_id`, `reclaimer_id`, `reclamations_id`, `status`) VALUES
(7, 'RECL_0X22_3_1', '2019-05-08', NULL, 10, 'RC8', 1, 3, NULL, 0),
(8, 'RECL_0X22_3_1', '2019-05-08', NULL, 10, 'RC8', 1, 3, NULL, 1),
(9, 'RECL_0X23_3_1', '2019-05-08', '2019-05-13', 10, 'RC8', 1, 3, NULL, 2),
(11, 'RECL_0X25_3_1', '2019-05-08', NULL, 10, 'RC8', 1, 3, NULL, NULL),
(12, 'RECL_0X26_3_1', '2019-05-08', NULL, 10, 'RC8', 1, 3, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_role`
--
ALTER TABLE `app_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_c9vam58sxsparp1djngaittd6` (`role_name`);

--
-- Indexes for table `app_user`
--
ALTER TABLE `app_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_3k4cplvh82srueuttfkwnylq0` (`username`),
  ADD UNIQUE KEY `UK_1j9d9a06i600gd43uu3km82jw` (`email`);

--
-- Indexes for table `app_user_roles`
--
ALTER TABLE `app_user_roles`
  ADD KEY `FK1pfb2loa8so5oi6ak7rh6enva` (`roles_id`),
  ADD KEY `FKkwxexnudtp5gmt82j0qtytnoe` (`app_user_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_projects`
--
ALTER TABLE `client_projects`
  ADD PRIMARY KEY (`client_id`,`projects_id`),
  ADD UNIQUE KEY `UK_3e59nap4h37q1adqsjm2nemkq` (`projects_id`);

--
-- Indexes for table `client_reclamations`
--
ALTER TABLE `client_reclamations`
  ADD PRIMARY KEY (`client_id`,`reclamations_id`),
  ADD UNIQUE KEY `UK_s0os189eva1u2fnocwahbjexu` (`reclamations_id`);

--
-- Indexes for table `hibernate_sequences`
--
ALTER TABLE `hibernate_sequences`
  ADD PRIMARY KEY (`sequence_name`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK8nw995uro0115f1go0dmrtn2d` (`client_id`);

--
-- Indexes for table `reclamation`
--
ALTER TABLE `reclamation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKmoir7qs5jjwl8knajdwgg9mjq` (`project_id`),
  ADD KEY `FK1iryxvs81kctqbuh2ly2pdigx` (`reclaimer_id`),
  ADD KEY `FKmsh0xfk8urrdkpd7si7s2s429` (`reclamations_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_role`
--
ALTER TABLE `app_role`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `app_user`
--
ALTER TABLE `app_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `reclamation`
--
ALTER TABLE `reclamation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `app_user_roles`
--
ALTER TABLE `app_user_roles`
  ADD CONSTRAINT `FK1pfb2loa8so5oi6ak7rh6enva` FOREIGN KEY (`roles_id`) REFERENCES `app_role` (`id`),
  ADD CONSTRAINT `FKkwxexnudtp5gmt82j0qtytnoe` FOREIGN KEY (`app_user_id`) REFERENCES `app_user` (`id`);

--
-- Constraints for table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `FK2re8ltj5jewc835q5d1id08i9` FOREIGN KEY (`id`) REFERENCES `app_user` (`id`);

--
-- Constraints for table `client_projects`
--
ALTER TABLE `client_projects`
  ADD CONSTRAINT `FK3i9oo3vnblvtb4237agp3mlkn` FOREIGN KEY (`projects_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `FKbqu97yh0w6eipwh6xj5vm6vw0` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Constraints for table `client_reclamations`
--
ALTER TABLE `client_reclamations`
  ADD CONSTRAINT `FKj3f30ds5q3gq9gixq1r2gwtci` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `FKrk1386cpmo5srowube1d7g0fs` FOREIGN KEY (`reclamations_id`) REFERENCES `reclamation` (`id`);

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `FK8nw995uro0115f1go0dmrtn2d` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Constraints for table `reclamation`
--
ALTER TABLE `reclamation`
  ADD CONSTRAINT `FK1iryxvs81kctqbuh2ly2pdigx` FOREIGN KEY (`reclaimer_id`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `FKmoir7qs5jjwl8knajdwgg9mjq` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `FKmsh0xfk8urrdkpd7si7s2s429` FOREIGN KEY (`reclamations_id`) REFERENCES `client` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
