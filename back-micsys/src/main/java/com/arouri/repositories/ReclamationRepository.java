package com.arouri.repositories;

import com.arouri.models.Reclamation;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;

/**
 * Created by Nidhal on 08/05/2019.
 */
@RepositoryRestController
public interface ReclamationRepository extends JpaRepository<Reclamation, Long> {

}
