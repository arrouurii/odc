package com.arouri.repositories;

import com.arouri.models.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Nidhal on 13/03/2019.
 */
@Transactional

public interface AppRoleRepository extends JpaRepository<AppRole, Long> {
    AppRole findByRoleName(String role);
}
