package com.arouri.repositories;

import com.arouri.models.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;

/**
 * Created by Nidhal on 07/05/2019.
 */
@RepositoryRestController
public interface ProjectRepository extends JpaRepository<Project, Long> {
}
