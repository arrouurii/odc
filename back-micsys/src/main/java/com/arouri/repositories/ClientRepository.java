package com.arouri.repositories;

import com.arouri.models.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by Nidhal on 06/05/2019.
 */
@RepositoryRestController
@Transactional
public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findClientByUsername(@Param("username") String username);
    Page<Client> findClientByFirstName(@Param("firstName") String firstName, Pageable pageable);
    Page<Client> findClientByFirstNameContains(@Param("keyword") String keyword, Pageable pageable);
    Page<Client> findClientByLastNameContains(@Param("keyword") String keyword, Pageable pageable);
    Page<Client> findClientByFirstNameContainsOrLastNameContains(@Param("keyword") String keyword,@Param("keyword") String key, Pageable pageable);
}
