package com.arouri.service;

import com.arouri.dto.ProjectDto;
import com.arouri.models.Project;

/**
 * Created by Nidhal on 07/05/2019.
 */
public interface ProjectService {
    Project saveProject(ProjectDto projectDto);
}
