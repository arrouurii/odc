package com.arouri.service.impl;

import com.arouri.dto.ReclamationDto;
import com.arouri.enums.ReclamationStatus;
import com.arouri.models.Client;
import com.arouri.models.Project;
import com.arouri.models.Reclamation;
import com.arouri.repositories.ClientRepository;
import com.arouri.repositories.ProjectRepository;
import com.arouri.repositories.ReclamationRepository;
import com.arouri.service.ReclamationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Nidhal on 08/05/2019.
 */
@Service
@Transactional
public class ReclamationServiceImpl implements ReclamationService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ReclamationRepository reclamationRepository;

    @Override
    public Reclamation saveReclamation(ReclamationDto data) {
        Reclamation reclamation = new Reclamation();
        reclamation.setName(data.getName());
        reclamation.setReclamationDate(new Date());
        reclamation.setType(data.getType());
        reclamation.setSeverity(data.getSeverity());
        reclamation.setStatus(ReclamationStatus.New);
        Project project = projectRepository.findById(data.getProjectId()).orElseThrow(
                () -> new ResourceNotFoundException("PR::404 Not Found !")
        );

        Client client = project.getClient();
        if (client == null) {
            throw new ResourceNotFoundException("CL::404 Not Found !");
        }
        client.getReclamations().add(reclamation);
        reclamation.setProject(project);
        reclamation.setReclaimer(client);

        return reclamationRepository.save(reclamation);
    }

    @Override
    public ResponseEntity delete(Long id) {
        Reclamation reclamation = reclamationRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("REC::404 NOT FOUND"));
        reclamation.getReclaimer().getReclamations().remove(reclamation);
        reclamation.setReclaimer(null);
        reclamation.setProject(null);
        reclamationRepository.save(reclamation);
        reclamationRepository.delete(reclamation);

        return ResponseEntity.noContent().build();
    }
}
