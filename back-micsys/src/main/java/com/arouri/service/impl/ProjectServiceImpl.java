package com.arouri.service.impl;

import com.arouri.dto.ProjectDto;
import com.arouri.models.Client;
import com.arouri.models.Project;
import com.arouri.repositories.ClientRepository;
import com.arouri.repositories.ProjectRepository;
import com.arouri.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Nidhal on 08/05/2019.
 */
@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    ClientRepository clientRepository;
    @Override
    public Project saveProject(ProjectDto data) {
        Project project = new Project();

        project.setName(data.getName());
        project.setReleaseDate(data.getReleaseDate());
        project.setLastUpdate(data.getLastUpdate());
        project.setGuaranteeDurationInMonths(data.getGuaranteeDurationInMonths());
        project.setVersion(data.getVersion());

        Client client = clientRepository.findClientByUsername(data.getClientName());
        if (client == null) throw new RuntimeException("404 Not Found, There is no client with the name " + client.getUsername());
        client.getProjects().add(project);
        // project.setClient(client);

        return projectRepository.save(project);
    }
}
