package com.arouri.service.impl;

import com.arouri.dto.ClientRegistrationForm;
import com.arouri.models.AppRole;
import com.arouri.models.AppUser;
import com.arouri.models.Client;
import com.arouri.repositories.AppRoleRepository;
import com.arouri.repositories.AppUserRepository;
import com.arouri.repositories.ClientRepository;
import com.arouri.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;

/**
 * Created by Nidhal on 06/05/2019.
 */
@Service
@Transactional
public class ClientAccountServiceImpl implements AccountService<Client, ClientRegistrationForm> {
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    AppRoleRepository roleRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Client saveUser(ClientRegistrationForm data) {
        String username = data.getUsername();
        AppUser user = this.findUserByUsername(username);
        if (user != null) throw new RuntimeException("This user already exists");
        String password = data.getPassword();
        String confirmPassword = data.getConfirmPassword();

        if (! password.equals(confirmPassword))
            throw new RuntimeException("Confirm password exception");

        AppUser u = new AppUser();
        u.setUsername(username);
        u.setFirstName(data.getFirstName());
        u.setLastName(data.getLastName());
        u.setInscriptionDate(new Date());
        u.setLastConnexion(new Date());
        u.setPassword(
                bCryptPasswordEncoder.encode(
                        password
                )
        );
        u.setActivate(true);
        u.setGender(data.getGender());
        u.setEmail(data.getEmail());
        Client client = new Client(u, data.getPosition(), data.getType(), data.getEnterpriseName());
        // -------------------------------
        Client ur = clientRepository.save(client);
        this.addRoleToUser(username, "USER");

        return ur;
    }

    @Override
    public AppRole saveRole(AppRole appRole) {
        return roleRepository.save(appRole);
    }

    @Override
    public Client findUserByUsername(String username) {
        return clientRepository.findClientByUsername(username);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        Client user = clientRepository.findClientByUsername(username);
        System.out.println(user.getUsername());
        AppRole role = roleRepository.findByRoleName(roleName);
        user.getRoles().add(role);
    }

    @Override
    public Collection<Client> getAllUsers() {
        return clientRepository.findAll();
    }
}
