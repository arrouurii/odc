package com.arouri.service.impl;

import com.arouri.dto.RegistrationForm;
import com.arouri.models.AppRole;
import com.arouri.models.AppUser;
import com.arouri.models.AppUser;
import com.arouri.repositories.AppRoleRepository;
import com.arouri.repositories.AppUserRepository;
import com.arouri.repositories.AppUserRepository;
import com.arouri.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;

/**
 * Created by Nidhal on 13/03/2019.
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService<AppUser, RegistrationForm>{
    @Autowired
    AppUserRepository appUserRepository;
    @Autowired
    AppRoleRepository roleRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;


    // =======================================
    @Override
    public AppUser saveUser(RegistrationForm data) {
        String username = data.getUsername();
        AppUser user = this.findUserByUsername(username);
        if (user != null) throw new RuntimeException("This user already exists");
        String password = data.getPassword();
        String confirmPassword = data.getConfirmPassword();

        if (! password.equals(confirmPassword))
            throw new RuntimeException("Confirm password exception");
        AppUser u = new AppUser();

        u.setUsername(username);
        u.setFirstName(data.getFirstName());
        u.setLastName(data.getLastName());
        u.setInscriptionDate(new Date());
        u.setLastConnexion(new Date());
        u.setPassword(
                bCryptPasswordEncoder.encode(
                        password
                )
        );
        u.setActivate(true);
        // -------------------------------
        AppUser ur = appUserRepository.save(u);

        this.addRoleToUser(username, "USER");

        return ur;
    }

    @Override
    public AppRole saveRole(AppRole appRole) {
        return roleRepository.save(appRole);
    }

    @Override
    public AppUser findUserByUsername(String username) {
        return appUserRepository.findAppUserByUsername(username);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        AppUser user = appUserRepository.findAppUserByUsername(username);
        System.out.println(user.getUsername());
        AppRole role = roleRepository.findByRoleName(roleName);
        user.getRoles().add(role);
    }

    @Override
    public Collection<AppUser> getAllUsers() {
        return appUserRepository.findAll();
    }
}
