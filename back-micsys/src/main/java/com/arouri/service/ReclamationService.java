package com.arouri.service;

import com.arouri.dto.ReclamationDto;
import com.arouri.models.Reclamation;
import org.springframework.http.ResponseEntity;

/**
 * Created by Nidhal on 08/05/2019.
 */
public interface ReclamationService {
    Reclamation saveReclamation(ReclamationDto data);
    ResponseEntity delete(Long id);
}
