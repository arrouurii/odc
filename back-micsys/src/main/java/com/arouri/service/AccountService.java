package com.arouri.service;

import com.arouri.dto.RegistrationForm;
import com.arouri.models.AppRole;
import com.arouri.models.AppUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Created by Nidhal on 13/03/2019.
 */
public interface AccountService<T extends AppUser, R extends RegistrationForm> {
    T saveUser(R data);
    AppRole saveRole(AppRole appRole);
    T findUserByUsername(String username);
    void addRoleToUser(String username, String roleName);
    // --------------------------------------------------
    Collection<T> getAllUsers();
}
