package com.arouri.enums;

/**
 * Created by Nidhal on 09/05/2019.
 */
public enum ReclamationStatus {
    New,
    InProgress,
    Closed
}
