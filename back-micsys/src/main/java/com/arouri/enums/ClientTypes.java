package com.arouri.enums;

/**
 * Created by Nidhal on 13/05/2019.
 */
public enum ClientTypes {
    Representative,
    Owner
}
