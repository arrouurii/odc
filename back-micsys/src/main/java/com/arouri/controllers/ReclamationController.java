package com.arouri.controllers;

import com.arouri.dto.ReclamationDto;
import com.arouri.models.Reclamation;
import com.arouri.repositories.ReclamationRepository;
import com.arouri.service.ReclamationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Nidhal on 08/05/2019.
 */
@RestController
@CrossOrigin("*")
public class ReclamationController {
    @Autowired
    ReclamationService reclamationService;

    @PostMapping("/reclamations/save")
    Reclamation save(@RequestBody ReclamationDto data) {
        return reclamationService.saveReclamation(data);
    }

    @DeleteMapping(value = "/reclamations/{id}")
    ResponseEntity delete(@PathVariable Long id) {
        return reclamationService.delete(id);
    }
}
