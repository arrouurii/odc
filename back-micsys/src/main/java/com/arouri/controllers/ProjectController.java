package com.arouri.controllers;

import com.arouri.dto.ProjectDto;
import com.arouri.models.Project;
import com.arouri.repositories.ProjectRepository;
import com.arouri.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Nidhal on 08/05/2019.
 */
@RestController
@CrossOrigin("*")
public class ProjectController {
    @Autowired
    ProjectService projectService;

    @PostMapping("/projects/save")
    Project save(@RequestBody ProjectDto data) {
        return projectService.saveProject(data);
    }
}
