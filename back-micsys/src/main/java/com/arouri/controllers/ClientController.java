package com.arouri.controllers;

import com.arouri.dto.ClientRegistrationForm;
import com.arouri.models.Client;

import com.arouri.repositories.ClientRepository;
import com.arouri.service.impl.ClientAccountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Created by Nidhal on 13/03/2019.
 */
@RestController
@Transactional
@CrossOrigin("*")
public class ClientController {
//    @Autowired
//    @Qualifier(value = "clientAccountService")
    private final ClientAccountServiceImpl accountService;
//    @Autowired
    private final ClientRepository clientRepository;

    @Autowired
    public ClientController(ClientAccountServiceImpl accountService, ClientRepository clientRepository) {
        this.accountService = accountService;
        this.clientRepository = clientRepository;
    }

    // ==================================
    @PostMapping("/clients/signup")
    public Client signUp(@RequestBody ClientRegistrationForm data) {

        return accountService.saveUser(data);
    }
}

