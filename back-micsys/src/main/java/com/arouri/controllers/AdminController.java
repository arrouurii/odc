package com.arouri.controllers;

import com.arouri.constants.SecretQuestion;
import com.arouri.models.AppRole;
import com.arouri.models.AppUser;
import com.arouri.repositories.AppUserRepository;
import com.arouri.security.config.constants.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

/**
 * Created by Nidhal on 09/05/2019.
 */
@RestController
@CrossOrigin("*")
public class AdminController {
    @Autowired
    AppUserRepository userRepository;
    @PostMapping("/admin/ami")
    ResponseEntity<Boolean> isAdmin(@RequestBody String username) {
        AppUser appUser = userRepository.findAppUserByUsername(username);
        if (appUser == null) throw new ResourceNotFoundException("User::404 NotFound");
        try {
            AppRole role = appUser.getRoles().stream().filter(appRole -> appRole.getRoleName().equals("ADMIN")).findFirst().get();
            if (role.getRoleName().equalsIgnoreCase("ADMIN")) {
                return ResponseEntity.ok().body(true);
            }
        }
        catch (NoSuchElementException e) {
            System.out.println(e.getMessage() + ", cause : " + e.getCause());
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/admin/challenge")
    ResponseEntity<Boolean> isAGoodAnswer(@RequestBody Double answer) {
        System.out.println("===" + answer);
        System.out.println("+++" + SecretQuestion.SECRETQESTIONANSWER);
        System.out.println("---" + (answer.compareTo(SecretQuestion.SECRETQESTIONANSWER)));
        if (answer.compareTo(SecretQuestion.SECRETQESTIONANSWER) == 0)
            return ResponseEntity.ok(true);
        return ResponseEntity.badRequest().build();
    }
}
