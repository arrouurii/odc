package com.arouri;

import com.arouri.dto.ClientRegistrationForm;
import com.arouri.dto.RegistrationForm;
import com.arouri.models.AppRole;
import com.arouri.repositories.ClientRepository;
import com.arouri.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Date;

@SpringBootApplication
@EnableSwagger2
public class BackMicsysApplication {
	public final ClientRepository clientRepository;

	@Autowired
	public BackMicsysApplication(ClientRepository clientRepository) {
		this.clientRepository = clientRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(BackMicsysApplication.class, args);
	}

	@Bean
	CommandLineRunner start(@Qualifier("accountServiceImpl") AccountService accountService) {
		return args -> {
			/*accountService.saveRole(new AppRole(null, "USER"));
			accountService.saveRole(new AppRole(null, "ADMIN"));
			accountService.saveUser(
					new ClientRegistrationForm(
							"admin",
							"test",
							"test",
							"Admin",
							"Admin",
							new Date(),
							new Date(),
							"",""
					)
			);
			accountService.addRoleToUser("admin", "ADMIN");*/
		};
	}
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
