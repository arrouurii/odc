package com.arouri.dto;

import lombok.*;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Created by Nidhal on 08/05/2019.
 */
@Data @AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class ProjectDto {
    private String name;
    private Date releaseDate;
    private Date lastUpdate;
    private String version;
    private Integer guaranteeDurationInMonths;

    private String clientName;
}
