package com.arouri.dto;

import com.arouri.enums.ReclamationStatus;
import com.arouri.models.Client;
import com.arouri.models.Project;
import lombok.*;

import java.util.Date;

/**
 * Created by Nidhal on 08/05/2019.
 */
@Data
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
public class ReclamationDto {
    private String name;
    private String type;
    private Integer severity;
    private String username;
    private Long projectId;
    private ReclamationStatus status;
}
