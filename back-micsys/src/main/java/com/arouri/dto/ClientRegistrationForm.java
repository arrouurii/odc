package com.arouri.dto;

import com.arouri.enums.ClientPosition;
import com.arouri.enums.ClientTypes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import java.util.Date;

/**
 * Created by Nidhal on 06/05/2019.
 */
@Data
@NoArgsConstructor
public class ClientRegistrationForm extends RegistrationForm {
    @Email
    private String email;
    private String enterpriseName;
    private int gender;
    private ClientTypes type;
    private ClientPosition position;

    public ClientRegistrationForm(ClientPosition position, ClientTypes type, String email, int gender, String enterpriseName) {
        this.position = position;
        this.type = type;
        this.email = email;
        this.gender = gender;
        this.enterpriseName = enterpriseName;
    }

    public ClientRegistrationForm(
            String username,
            String password,
            String confirmPassword,
            String firstName,
            String lastName,
            Date inscriptionDate,
            Date lastConnexion,
            ClientPosition position,
            ClientTypes type,
            String email,
            int gender,
            String enterpriseName
    ) {
        super(username, password, confirmPassword, firstName, lastName, inscriptionDate, lastConnexion);
        this.position = position;
        this.type = type;
        this.email = email;
        this.gender = gender;
        this.enterpriseName = enterpriseName;
    }
}
