package com.arouri.dto;

import lombok.*;

import java.util.Date;

/**
 * Created by Nidhal on 13/03/2019.
 */
@Data @Getter @Setter @AllArgsConstructor @NoArgsConstructor
public abstract class RegistrationForm {
    private String username;
    private String password;
    private String confirmPassword;
    private String firstName;
    private String lastName;
//    private String type;

//    private UsersTypes usersType;

    private Date inscriptionDate;
    private Date lastConnexion;
}
