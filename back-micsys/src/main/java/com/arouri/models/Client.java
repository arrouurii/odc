package com.arouri.models;

import com.arouri.enums.ClientPosition;
import com.arouri.enums.ClientTypes;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Nidhal on 06/05/2019.
 */
@EqualsAndHashCode(callSuper = true, exclude = {"reclamations", "projects"})
@Entity
@Data
@NoArgsConstructor
public class Client extends AppUser {
     private ClientPosition position;
     private ClientTypes type;
     private String enterpriseName = "N.A";
     @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
     @JsonBackReference @JsonIgnore
     Set<Reclamation> reclamations = new HashSet<>();
     @OneToMany(fetch = FetchType.LAZY)
     @JsonBackReference @JsonIgnore
     private Set<Project> projects = new HashSet<>();

    public Client(ClientPosition position, ClientTypes type, String enterpriseName) {
        super();
        this.position = position;
        this.type = type;
        this.enterpriseName = enterpriseName;
    }

    public Client(AppUser appUser, ClientPosition position, ClientTypes type, String enterpriseName) {
        super(appUser);
        this.setType(type);
        this.setPosition(position);
        this.enterpriseName = enterpriseName;
    }
}