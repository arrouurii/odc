package com.arouri.models;

import lombok.*;

import javax.persistence.*;

/**
 * Created by Nidhal on 13/03/2019.
 */
@Entity
@Data @AllArgsConstructor @NoArgsConstructor @Getter @Setter
public class AppRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(unique = true)
    String roleName;
}
