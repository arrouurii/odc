package com.arouri.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Nidhal on 13/03/2019.
 */
@Entity
@Data @AllArgsConstructor @NoArgsConstructor @Getter @Setter
@Inheritance(strategy = InheritanceType.JOINED)
public class AppUser {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(unique = true) @NotNull
    String username;
    @Column(unique = true)
    @Email
    String email;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) @NotNull
    String password;
    @NotBlank @NotNull
    String firstName;
    @NotBlank @NotNull
    String lastName;
    private boolean activate;
    @NotNull
    private int gender;
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date inscriptionDate;
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date lastConnexion;
    @ManyToMany(fetch = FetchType.EAGER) // EAGER : each time we load a user, it's own roles will be loaded
    Collection<AppRole> roles = new ArrayList<>();

    public AppUser(AppUser appUser) {
        this.username = appUser.getUsername();
        this.email = appUser.getEmail();
        this.password = appUser.getPassword();
        this.firstName = appUser.getFirstName();
        this.lastName = appUser.getLastName();
        this.activate = appUser.isActivate();
        this.gender = appUser.getGender();
        this.inscriptionDate = appUser.getInscriptionDate();
        this.lastConnexion = appUser.getLastConnexion();
        this.roles.addAll(appUser.getRoles());
    }
}
