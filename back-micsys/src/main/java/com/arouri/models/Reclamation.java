package com.arouri.models;

import com.arouri.enums.ReclamationStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Nidhal on 08/05/2019.
 */
@Entity
@Data @AllArgsConstructor @NoArgsConstructor @Getter @Setter
@EqualsAndHashCode(exclude = {"project", "reclaimer"})
public class Reclamation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String type;
    private ReclamationStatus status;
    @Temporal(TemporalType.DATE)
    private Date reclamationDate;
    @Temporal(TemporalType.DATE)
    private Date resolutionDate;
    private Integer severity;
    @ManyToOne @JsonManagedReference
    Client reclaimer;
    @ManyToOne @JsonManagedReference
    Project project;
}
