package com.arouri.models;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Nidhal on 07/05/2019.
 */
@Entity
@Data @Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Temporal(TemporalType.DATE)
    private Date releaseDate;
    @Temporal(TemporalType.DATE)
    private Date lastUpdate;
    private String version;
    private Integer guaranteeDurationInMonths;
    @ManyToOne
    private Client client;
}
