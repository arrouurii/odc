package com.odc.demo.controllers;

import com.odc.demo.models.Particular;
import com.odc.demo.models.Resume;
import com.odc.demo.repositories.ParticularRepository;
import com.odc.demo.repositories.ResumeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@RestController("/particular")
@CrossOrigin("*")
@Transactional()
public class ParticularController {
    @Autowired
    ParticularRepository particularRepository;
    @Autowired
    ResumeRepository resumeRepository;
    @PostMapping()
    void addParticular(@RequestBody Particular particular) {
        particularRepository.save(particular);
    }
    @GetMapping()
    List<Particular> findAll() {
        return particularRepository.findAll();
    }
    @PatchMapping("/:id")
    void updateParticular(@PathVariable Long id ,@RequestBody Particular particular) {

        Particular persistedParticular = particularRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        particular.setId(persistedParticular.getId());
        particularRepository.save(particular);
    }
    @PatchMapping("/:id/updateResume")
    void updateParticularResume(@PathVariable Long id ,@RequestBody Resume resume) {
        Particular persistedParticular = particularRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        persistedParticular.setResume(resume);
        particularRepository.save(persistedParticular);
    }

}
