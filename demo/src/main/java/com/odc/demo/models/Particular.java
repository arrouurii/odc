package com.odc.demo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

@Entity
@Setter @Getter @AllArgsConstructor
public class Particular extends AppClient {
    @Temporal(TemporalType.DATE)
    Date birthDay;

    @OneToOne
    Resume resume;
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    Set<Formation> formations;
}
