package com.odc.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Enterprise extends AppClient {
    @Column(unique = true)
    String secondTel;
    String city;
    String country;
}
