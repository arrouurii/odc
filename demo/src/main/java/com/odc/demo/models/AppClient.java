package com.odc.demo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.Collection;

@Setter @Getter @NoArgsConstructor @AllArgsConstructor
@Entity
public class AppClient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Email @Column(unique = true)
    String email;
    String password;
    String firstname;
    String lastname;
    String address;
    @Column(unique = true)
    String tel;

    @ManyToMany(fetch = FetchType.EAGER) // EAGER : each time we load a user, it's own roles will be loaded
    Collection<AppRole> roles = new ArrayList<>();

}
