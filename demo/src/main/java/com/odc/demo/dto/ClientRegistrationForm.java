package com.odc.demo.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import java.util.Date;

/**
 * Created by Nidhal on 06/05/2019.
 */
@Data
@NoArgsConstructor
public class ClientRegistrationForm extends RegistrationForm {
    @Email
    private String email;
    private String enterpriseName;
    private int gender;

    public ClientRegistrationForm(String email, int gender, String enterpriseName) {
        this.email = email;
        this.gender = gender;
        this.enterpriseName = enterpriseName;
    }

    public ClientRegistrationForm(
            String username,
            String password,
            String confirmPassword,
            String firstName,
            String lastName,
            Date inscriptionDate,
            Date lastConnexion,
            String email,
            int gender,
            String enterpriseName
    ) {
        super(username, password, confirmPassword, firstName, lastName, inscriptionDate, lastConnexion);
        this.email = email;
        this.gender = gender;
        this.enterpriseName = enterpriseName;
    }
}
