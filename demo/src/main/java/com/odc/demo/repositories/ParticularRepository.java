package com.odc.demo.repositories;

import com.odc.demo.models.Particular;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParticularRepository extends JpaRepository<Particular, Long> {

}
