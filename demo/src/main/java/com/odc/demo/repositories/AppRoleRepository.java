package com.odc.demo.repositories;

import com.odc.demo.models.AppClient;
import com.odc.demo.models.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppRoleRepository extends JpaRepository<AppRole, Long> {
    AppRole findByRoleName(String name);
}
