package com.odc.demo.repositories;

import com.odc.demo.models.AppClient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppClientRepository extends JpaRepository<AppClient, Long> {
    AppClient findAppClientByEmail(String email);
}
