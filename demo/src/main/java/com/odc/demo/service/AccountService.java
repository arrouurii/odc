package com.odc.demo.service;


import com.odc.demo.dto.RegistrationForm;
import com.odc.demo.models.AppClient;
import com.odc.demo.models.AppRole;

import java.util.Collection;

/**
 * Created by Nidhal on 13/03/2019.
 */
public interface AccountService<T extends AppClient, R extends RegistrationForm> {
    T saveUser(R data);
    AppRole saveRole(AppRole appRole);
    T findUserByUsername(String username);
    void addRoleToUser(String username, String roleName);
    // --------------------------------------------------
    Collection<T> getAllUsers();
}
