package com.odc.demo.service.impl;

//import com.arouri.dto.RegistrationForm;
//import com.arouri.models.AppRole;
//import com.arouri.models.AppClient;
//import com.arouri.repositories.AppRoleRepository;
//import com.arouri.repositories.AppClientRepository;
//import com.arouri.service.AccountService;
import com.odc.demo.dto.RegistrationForm;
import com.odc.demo.models.AppClient;
import com.odc.demo.models.AppRole;
import com.odc.demo.repositories.AppClientRepository;
import com.odc.demo.repositories.AppRoleRepository;
import com.odc.demo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;

/**
 * Created by Nidhal on 13/03/2019.
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService<AppClient, RegistrationForm> {
    @Autowired
    AppClientRepository appClientRepository;
    @Autowired
    AppRoleRepository roleRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;


    // =======================================
    @Override
    public AppClient saveUser(RegistrationForm data) {
        String username = data.getUsername();
        AppClient user = this.findUserByUsername(username);
        if (user != null) throw new RuntimeException("This user already exists");
        String password = data.getPassword();
        String confirmPassword = data.getConfirmPassword();

        if (! password.equals(confirmPassword))
            throw new RuntimeException("Confirm password exception");
        AppClient u = new AppClient();

        u.setEmail(username);
        u.setFirstname(data.getFirstName());
        u.setLastname(data.getLastName());
//        u.setInscriptionDate(new Date());
//        u.setLastConnexion(new Date());
        u.setPassword(
                bCryptPasswordEncoder.encode(
                        password
                )
        );
//        u.setActivate(true);
        // -------------------------------
        AppClient ur = appClientRepository.save(u);

        this.addRoleToUser(username, "USER");

        return ur;
    }

    @Override
    public AppRole saveRole(AppRole appRole) {
        return roleRepository.save(appRole);
    }


    @Override
    public AppClient findUserByUsername(String email) {
        return appClientRepository.findAppClientByEmail(email);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        AppClient user = appClientRepository.findAppClientByEmail(username);
        System.out.println(user.getEmail());
        AppRole role = roleRepository.findByRoleName(roleName);
        user.getRoles().add(role);
    }

    @Override
    public Collection<AppClient> getAllUsers() {
        return appClientRepository.findAll();
    }
}
